import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import PostsnComments from './PostsnComments';

const Posts = () => {
  let posts_json = [
    {
      user: 'anand',
      title: 'First Post',
      body: 'This is my First Post here',
      comments: {
        akhil: {
          comment1: 'excellent posts',
          comment2: 'can do better than this',
        },
      },
    },
    {
      user: 'raghu',
      title: 'Second Post',
      body: 'This is my Second Post here',
      comments: {
        akhil: {
          comment1: 'good posts',
          comment2: 'this is okay',
        },
      },
    },
  ];
  return posts_json.map((v, k) => {
    return (
      <View>
        <TouchableOpacity
          style={styles.container}
          delayPressIn={0}
          onPress={() => alert('navigate to the PostsnComments view')}>
          <View key={k}>
            <Text> Posted By: {v.user} </Text>
            <Text> Title : {v.title} </Text>
            <Text> Description : {v.body} </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  });
};
export default Posts;

const styles = StyleSheet.create({
  container: {
    borderColor: 'green',
    borderWidth: 2,
    backgroundColor: '#2196F3',
    padding: 40,
  },
});
