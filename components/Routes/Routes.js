/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import {NativeRouter, Route} from 'react-router-native';
import Posts from '../Posts';
import PostsnComments from '../PostsnComments';

const App = () => {
  return (
    <View>
      <NativeRouter>
        <Route exact path="/" component={Posts} />
        <Route path="/about" component={PostsnComments} />
      </NativeRouter>
    </View>
  );
};
export default App;
