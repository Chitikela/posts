/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, Text} from 'react-native';

const PostLists = props => {
  console.log('hellow');
  console.log(props.data.body);
  return (
    <View>
      <Text> Posted By: {props.data.user} </Text>
      <Text> Title : {props.data.title} </Text>
      <Text> Body : {props.data.body} </Text>
    </View>
  );
};
export default PostLists;
